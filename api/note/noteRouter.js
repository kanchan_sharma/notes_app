const noteRoutr = require("express").Router();
var noteControllers = require('../note/noteController');
const resHndlr = require("../global/responder");

noteRoutr.route("/crateNoteDetails")
.post([], function(req,res){
	
	noteControllers.crateNoteDetails(req,res);
})
noteRoutr.route("/updateNoteDetails/:_id")
.post([], function(req,res){
	
	noteControllers.updateNoteDetails(req,res);
})
noteRoutr.route("/searchNoteByTitile")
.post([], function(req,res){
	
	noteControllers.searchNoteByTitile(req,res);
})
noteRoutr.route("/deleteNote")
.post([], function(req,res){
	
	noteControllers.deleteNote(req,res);
})
module.exports = noteRoutr
