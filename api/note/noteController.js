

const Constants = require('./noteConstants');

const resHndlr = require("../global/responder");
const Note = require("../note/note");

module.exports = {

	 crateNoteDetails: (req, res) => {
    console.log("req.body",req.body)
    if(!req.body.title ||!req.body.content)
      return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
    else{
      Note.create({'title':req.body.title,'content':req.body.content,'date_created':new Date().getTime()})
    .then((sucsses)=>{
     
       return resHndlr.apiResponder(req, res, 'Create Note Details Successfully.', 200,sucsses)
   })
    .catch((unsucsses)=>{
      console.log(unsucsses)
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
    })

    }  
},
updateNoteDetails:(req,res)=>{
  if(!req.params._id || !req.body.title)
    return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
  else{
  Note.findOneAndUpdate({'_id':req.params._id,'title':req.body.title},
    {$set:
      {date_updated:new Date().getTime()}
    },
    {new:true})
  .then((sucsses)=>{
     return resHndlr.apiResponder(req, res, 'Update Note Time Successfully.', 200,sucsses)
  })
  .catch((unsucsses)=>{
    return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
  })
}
},
searchNoteByTitile:(req,res)=>{
  if(!req.body.title)
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
   else{
    Note.findOne({'title':req.body.title})
  .then((sucsses)=>{
     return resHndlr.apiResponder(req, res, 'Single Note Details Successfully.', 200,sucsses)
  })
  .catch((unsucsses)=>{
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
  })

   }
  
},
deleteNote:(req,res)=>{
 if(!req.body.title)
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
   else{
  Note.findOne({'title':req.body.title},function(err,result){
    if(err)
      //console.log(err)
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400, 200)
    else{
      if(!result)
//console.log("data not found")
     return resHndlr.apiResponder(req, res, "Data Not found",200)
        else{
      Note.deleteOne(result)
      .then((sucsses)=>{
        return resHndlr.apiResponder(req, res, 'Delete Single Note Successfully.', 200)
      })
      .catch((unsucsses)=>{
       return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
      })
    }
    }
  })
  
}
}
	 
}