var mongoose = require('mongoose');

const Schema = mongoose.Schema;
var noteSchema = mongoose.Schema({
	title:{type:String, required: true,unique: true},
	content:{type:String},
	date_created:{type:Number},
	date_updated:{type:Number}
	

});
module.exports = mongoose.model('Notes',noteSchema);
