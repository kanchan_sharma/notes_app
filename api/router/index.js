var express = require('express'),
app = express()

const responseHandler = require('../global/responder');
//var userRouter =  require('../register/userRouter');
// var empRouter =  require('../employee/employeeRouter');
// var menuRouters =  require('../menu/menuRouter');
// var userRouters =  require('../user/userRouter');
var noteRouters =  require('../note/noteRouter');
//console.log(responseTime)
module.exports = function(app){
	//app.use('/mgt', userRouter);
	
	// app.use('/user',userRouters);
	// app.use('/menu', menuRouters);
	// app.use('/multichain/emp',empRouter);
	app.use('/note',noteRouters);
	
	app.use(responseHandler.apiResponder);
}