var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multichain = require('multichain-node');
var mongoose  = require('mongoose');
var responseTime = require('response-time')
var http = require('http');
mongoose.connect('mongodb://localhost/NOTE');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '10mb'}));
app.use(responseTime())
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, browser_id");
 next();
});



 require("./api/router")(app);//Router //If index.js featch deafualt 
console.log('Hello World...!');

app.listen(8007, () => console.log('Example app listening on port 8007!'));
console.log('your server will be started');
